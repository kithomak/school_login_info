import pandas as pd
pd.set_option("display.max_columns", 20)
import numpy as np
import pymysql


if __name__ == "__main__":
    host = "starplatform.c2ueg0anncqg.ap-southeast-1.rds.amazonaws.com"
    user = "starplatform"
    password = "star2016"
    port = 3306
    db = "fsschool"
    db_conn = pymysql.connect(host, user=user,port=port, passwd=password, db=db)

    query = """
    SELECT 
        domain as Domain, 
        english_name as English, 
        chinese_name as Chinese,
        exam_curriculums
    FROM schools
    WHERE
        is_demo = 0
        AND enable_exam_mode = 1
    """

    school_list_df = pd.read_sql(query, con=db_conn)
    school_list_df = school_list_df[school_list_df["exam_curriculums"].map(lambda x: x != "[]")].drop("exam_curriculums", axis=1)
    school_name_df = pd.read_excel("./data/school_info.xlsx")
    school_list_df = pd.DataFrame(school_list_df["Domain"]).merge(school_name_df, how="left")
    print(school_list_df)

    # for ind, row in school_name_df.iterrows:
    #     query = """
    #     UPDATE schools
    #     SET english_name = '{}', chinese_name = '{}'
    #     WHERE domain = '{}'
    #     """.format(row["English"], row["Chinese"], row["Domain"])
    #     db_conn.query(query)
    # db_conn.commit()


