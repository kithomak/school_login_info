from datetime import datetime
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import formatdate
from email import encoders
import json
import smtplib
import ssl
import os


def send_mail(send_from,
              send_to,
              subject,
              text,
              files,
              server,
              port,
              cc_to='',
              bcc_to='',
              username='',
              password='',
              isTls=True):
    msg = MIMEMultipart()
    msg['From'] = send_from
    msg['To'] = send_to
    msg['Cc'] = cc_to
    msg['Bcc'] = bcc_to
    msg['Date'] = formatdate(localtime = True)
    msg['Subject'] = subject
    msg.attach(MIMEText(text))
    
    print(msg)

    for path in files:
        print("Adding file", path)
        attachment = open(path, 'rb')
        filename = os.path.basename(path)
        part = MIMEBase('application', "octet-stream")
        part.set_payload(attachment.read())
        part.add_header('Content-Disposition', 'attachment', filename=filename)
        encoders.encode_base64(part)
        msg.attach(part)

    # context = ssl.create_default_context()
    #SSL connection only working on Python 3+
    with smtplib.SMTP(server, port) as smtp:
        smtp.ehlo()
        if isTls:
            smtp.starttls()
        smtp.login(sender_email, password)
        smtp.send_message(msg)
        print("Email sent.")
        

if __name__ == "__main__":
    os.system('bash generate_reports.sh')
    email_subject = "Login/Attempt Statistic for ICT/Trial/Paid/All School - {}".format(
        datetime.now().date()
    )
    
    message = """\
    Dear Viola,

    I have attached the school reports in this email.

    Regards,
    Kit-Ho Mak
    --
    Kit-Ho Mak
    Data Scientist
    Find Solution Ai Limited
    Rm3902-03, Singga Commercial Centre,
    No. 144-151 Connaught Road West,
    Sai Ying Pun, Hong Kong
    www.findsolutionai.com
    General: +852 2528 3322 | Fax: +852 2812 7332
    """

    files = [
        './reports/{}_AllSchool_info.xlsx'.format(datetime.now().date()),
        './reports/{}_ICT_info.xlsx'.format(datetime.now().date()),
        './reports/{}_Trial_info.xlsx'.format(datetime.now().date()),
        './reports/{}_Paid_info.xlsx'.format(datetime.now().date()),
    ]

    with open('./email_config.json', 'r') as f:
        email_config = json.load(f)
    sender_email = email_config['sender_email']
    receiver_emails = ", ".join(email_config['receiver_emails'])
    cc_emails = ", ".join(email_config['cc_emails'])
    bcc_emails = ", ".join(email_config['bcc_emails'])
    server = email_config['smtp_server']
    port = email_config['smtp_port']
    username = email_config['username']
    password = email_config['password']

    send_mail(
        send_from=sender_email,
        send_to="kit.ho@findsolutiongroup.com",
        cc_to="",
        bcc_to="",
        # send_to=receiver_emails,
        # cc_to=cc_emails,
        # bcc_to=bcc_emails,
        subject=email_subject,
        text=message,
        files=files,
        server=server,
        port=port,
        username=username,
        password=password,
        isTls=True
    )
