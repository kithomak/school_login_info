# Weekly Email for School/ICT/Trial Excel Reports

## Install Required Python Packages

Create and activate a Python envrionemnt.
```terminal
$ conda create --name email_env python=3.7
$ conda activate email_env
```

Install the required Python packages
```terminal
$ pip install -r requirements.txt
```

## Set the Email Configuration

Copy the `email_config.json.default` and name the copied file be `email_config.json`. The `email_config.json` is as follows:

```json
{
    "receiver_emails": [
        "viola@findsolutiongroup.com"
    ],
    "cc_emails":[
        "fanny@findsolutiongroup.com",
        "joe@findsolutiongroup.com"
    ],
    "bcc_emails":[
        "{your_email_address}"
    ],
    "smtp_server": "smtp.office365.com",
    "smtp_port": 587,
    "sender_email": "{your_email_address}",
    "username": "{your_email_address}",
    "password": "{your_email_password}"
}
```

Replace the email address and password in the `email_config.json`. Also, please modify the message inside `send_email.py` for your name and position.

## Send the Email

After the modification of the `email_config.json`, we can then send the email report by the following command:
```terminal
$ conda activate email_env
$ python send_email.py
```

You will observe the output message from the terminal similar to below:
```terminal
/Users/xxx/anaconda3/lib/python3.6/site-packages/requests/__init__.py:80: RequestsDependencyWarning: urllib3 (1.22) or chardet (2.3.0) doesn't match a supported version!
  RequestsDependencyWarning)
[NbConvertApp] Converting notebook AllSchool_LoginInfo.ipynb to notebook
[NbConvertApp] Executing notebook with kernel: python3
[NbConvertApp] Writing 92880 bytes to AllSchool_LoginInfo.ipynb
/Users/xxx/anaconda3/lib/python3.6/site-packages/requests/__init__.py:80: RequestsDependencyWarning: urllib3 (1.22) or chardet (2.3.0) doesn't match a supported version!
  RequestsDependencyWarning)
[NbConvertApp] Converting notebook ICT_LoginInfo.ipynb to notebook
[NbConvertApp] Executing notebook with kernel: python3
[NbConvertApp] Writing 73821 bytes to ICT_LoginInfo.ipynb
/Users/xxx/anaconda3/lib/python3.6/site-packages/requests/__init__.py:80: RequestsDependencyWarning: urllib3 (1.22) or chardet (2.3.0) doesn't match a supported version!
  RequestsDependencyWarning)
[NbConvertApp] Converting notebook Trial_LoginInfo.ipynb to notebook
[NbConvertApp] Executing notebook with kernel: python3
[NbConvertApp] Writing 71294 bytes to Trial_LoginInfo.ipynb
Content-Type: multipart/mixed; boundary="===============5761691198934108770=="
MIME-Version: 1.0
From: xxx@findsolutiongroup.com
To: fsai.xxx@gmail.com
Cc: 
Bcc: 
Date: Thu, 06 Jun 2019 11:47:21 +0800
Subject: Login/Attempt Statistic for ICT/Trial/All School - 2019-06-06

--===============5761691198934108770==
Content-Type: text/plain; charset="us-ascii"
MIME-Version: 1.0
Content-Transfer-Encoding: 7bit

    Dear Viola,

    I have attached the school reports in this email.

    Regards,
    {Your Name}
    --
    {Your Name}
    {Your Title}
    Find Solution Ai Limited
    Rm3902-03, Singga Commercial Centre,
    No. 144-151 Connaught Road West,
    Sai Ying Pun, Hong Kong
    www.findsolutionai.com
    General: +852 2528 3322 | Fax: +852 2812 7332
    
--===============5761691198934108770==--

Adding file ./reports/2019-06-06_AllSchool_info.xlsx
Adding file ./reports/2019-06-06_ICT_info.xlsx
Adding file ./reports/2019-06-06_Trial_info.xlsx
Email sent.
```