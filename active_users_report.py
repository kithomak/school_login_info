from datetime import timezone, datetime, timedelta
from dateutil.relativedelta import relativedelta

import argparse
import pandas as pd
pd.set_option("display.max_columns", 20)
import numpy as np
import pymysql
import pytz
from xlsxwriter.utility import xl_rowcol_to_cell


# global settings for db
host = "starplatform.c2ueg0anncqg.ap-southeast-1.rds.amazonaws.com"
user = "starplatform"
password = "star2016"
port = 3306
curriculum_db = "teacher-service"
curriculum_conn = pymysql.connect(host, user=user, port=port, passwd=password, db=curriculum_db)

auth_db = "auth"
auth_conn = pymysql.connect(host, user=user, port=port, passwd=password, db=auth_db)

school_db = "fsschool"
school_conn = pymysql.connect(host, user=user, port=port, passwd=password, db=school_db)


def get_paid_schools():
    """
    Get a list of domains for the paid schools.

    :return: A list of paid schools.
    """
    query = """
    SELECT
        schools.domain as school,
        schools.chinese_name,
        schools.english_name,
        school_type.is_primary_school
    FROM schools
    LEFT JOIN school_type ON schools.domain = school_type.domain
    LEFT JOIN school_paid ON schools.domain = school_paid.domain
    WHERE schools.is_demo = 0 AND school_paid.is_paid = 1 AND schools.disabled = 0
    """
    school_name_df = pd.read_sql(query, con=school_conn)
    school_name_df['school'] = school_name_df['school'].str.lower()
    school_name_df = school_name_df[school_name_df["chinese_name"] != ""].dropna(
        subset=["chinese_name", "english_name"]).reset_index(drop=True)
    return school_name_df


def get_ict_schools():
    """
    Get a list of domains for the trial schools.

    :return: A list of trial schools.
    """
    query = """
    SELECT
        schools.domain as school,
        schools.chinese_name,
        schools.english_name,
        school_type.is_primary_school
    FROM schools
    LEFT JOIN school_type ON schools.domain = school_type.domain
    WHERE schools.is_demo = 0 AND schools.enable_exam_mode = 1 AND schools.disabled = 0
    """
    school_name_df = pd.read_sql(query, con=school_conn)
    school_name_df['school'] = school_name_df['school'].str.lower()
    school_name_df = school_name_df[school_name_df["chinese_name"] != ""].dropna(
        subset=["chinese_name", "english_name"]).reset_index(drop=True)
    return school_name_df


def get_trial_schools():
    """
    Get a list of domains for the trial schools.

    :return: A list of trial schools.
    """
    query = """
    SELECT
        schools.domain as school,
        schools.chinese_name,
        schools.english_name,
        school_type.is_primary_school
    FROM schools
    LEFT JOIN school_type ON schools.domain = school_type.domain
    LEFT JOIN school_paid ON schools.domain = school_paid.domain
    WHERE schools.is_demo = 0 AND schools.disabled = 0 AND school_paid.is_paid = 0
    """
    school_name_df = pd.read_sql(query, con=school_conn)
    school_name_df['school'] = school_name_df['school'].str.lower()
    school_name_df = school_name_df[school_name_df["chinese_name"] != ""].dropna(
        subset=["chinese_name", "english_name"]).reset_index(drop=True)
    return school_name_df


def get_teacher_login_info(start_date=(datetime.today()-timedelta(7)).date().strftime("%Y-%m-%d"),
                           end_date=datetime.today().date().strftime("%Y-%m-%d")):
    """
    Get the login details for teachers.

    :return: Pandas dataframe of teacher login info.
    """
    query = """
    SELECT 
        SUBSTRING_INDEX(users.login_name,'.',1) as school,
        users.login_name as login_name,
        user_roles.role as role,
        MIN(CONVERT_TZ(oauth_codes.expires_at, 'GMT', 'Asia/Hong_Kong')) as first_login_time,
        MAX(CONVERT_TZ(oauth_codes.expires_at, 'GMT', 'Asia/Hong_Kong')) as latest_login_time,
        COUNT(oauth_codes.expires_at) as num_auth_code
    FROM users
    LEFT JOIN user_roles ON user_roles.user_id = users.id
    LEFT JOIN oauth_auth_codes as oauth_codes ON oauth_codes.user_id = users.id
    WHERE
        user_roles.role = 'teacher'
        AND users.login_name NOT LIKE '%school%'
        AND users.login_name NOT LIKE '%fsai%'
        AND CONVERT_TZ(oauth_codes.expires_at, 'GMT', 'Asia/Hong_Kong') >= "{}"
        AND CONVERT_TZ(oauth_codes.expires_at, 'GMT', 'Asia/Hong_Kong') < "{}"
    GROUP BY users.login_name
    """.format(start_date, end_date)

    teacher_login_df = pd.read_sql(query, con=auth_conn)
    return teacher_login_df


def get_students_attempts(start_date=(datetime.today()-timedelta(7)).date().strftime("%Y-%m-%d"),
                          end_date=datetime.today().date().strftime("%Y-%m-%d")):
    """
    Get a list of studeents attempts.

    :param: int num_days: Number of days of data to fetch.
    :return: Pandas dataframe of students attempts.
    """
    query = """
        SELECT 
            table1.school, 
            table1.user, 
            table1.`date`, 
            table1.count,
            table1.duration,
            table1.max_end,
            table2.is_primary_school 
        FROM
        (SELECT SUBSTRING_INDEX(profile.user,".",1) as school,
        profile.user as user,
        DATE(CONVERT_TZ(start, 'GMT', 'Asia/Hong_Kong')) as `date`,
        COUNT(*) as count,
        CONVERT_TZ(MAX(end), 'GMT', 'Asia/Hong_Kong') as max_end,
        SUM(UNIX_TIMESTAMP(end) - UNIX_TIMESTAMP(start)) as duration
        FROM `teacher-service`.students_attempt attempt
        INNER JOIN `teacher-service`.students_knowledgecomponent kc ON kc.id = attempt.kc_id
        INNER JOIN `teacher-service`.students_profile profile ON kc.profile_id = profile.id
        WHERE CONVERT_TZ(end, 'GMT', 'Asia/Hong_Kong') >= "{}"
        AND CONVERT_TZ(end, 'GMT', 'Asia/Hong_Kong') < "{}"
        AND CONVERT_TZ(start, 'GMT', 'Asia/Hong_Kong') >= "{}"
        AND CONVERT_TZ(start, 'GMT', 'Asia/Hong_Kong') < "{}"
        GROUP BY school, `date`, user) table1
        INNER JOIN fsschool.school_type table2 ON table1.school = table2.domain
        """.format(start_date, end_date, start_date, end_date)
    df = pd.read_sql(query, con=curriculum_conn)
    df = df.rename(columns={"user": "login_name"})
    return df


def get_school_last_attempt_time(schools):
    """
    Get the last attempt time for a given school.

    :param schools: school domain names, in an array.
    :return: Last attempt time of the school, None if no attempts can be found.
    """
    query = """
    SELECT 
    SUBSTRING_INDEX(profile.user,".",1) as school, 
    max(end) as last_active_time_student
    FROM `teacher-service`.students_attempt attempt
    INNER JOIN `teacher-service`.students_knowledgecomponent kc ON kc.id = attempt.kc_id
    INNER JOIN `teacher-service`.students_profile profile ON kc.profile_id = profile.id
    WHERE SUBSTRING_INDEX(profile.user,".",1) IN ('{}')
    group by school
    """.format("','".join(schools))
    df = pd.read_sql(query, con=curriculum_conn)
    return df


def get_schools_stats(teachers_df, students_df, school_list):
    """
    Get school statistics from list of attempts.

    :param teachers_df: df of teachers login details
    :param students_df: df of students attempt details
    :param school_list: df of schools to report (with Chinese and English name of the schools)
    :return: (df, inactive, dauc) -- A dataframe of school statistics, a df of inactive schools,
             and a df of daily active user count
    """
    school_set = set(school_list["school"])

    # target_teachers_df = teachers_df[teachers_df['school'].isin(school_set)]
    # target_teachers_df = target_teachers_df.sort_values(["school", "login_name"]).reset_index(drop=True)
    # target_teachers_stats = target_teachers_df.groupby("school").agg({
    #     "login_name": "nunique",
    #     "latest_login_time": "max",
    #     "role": "count"
    # })
    # target_teachers_stats = target_teachers_stats.rename(columns={
    #     "login_name": "num_active_teachers",
    #     "role": "num_logins_teacher",
    #     "latest_login_time": "last_active_time_teacher"
    # })
    # target_teachers_stats = target_teachers_stats.drop("num_logins_teacher", axis=1)

    target_students_df = students_df[students_df['school'].isin(school_set)]
    target_students_df = target_students_df.sort_values(["school", "login_name"]).reset_index(drop=True)

    target_schools_report = target_students_df.groupby("school").agg({
        "login_name": "nunique",
        "count": "sum",
        "duration": "sum",
        "max_end": "max"
    }).reset_index()
    target_schools_report = target_schools_report.rename(columns={
        "login_name": "num_active_students",
        "count": "num_attempts",
        "duration": "total_attempts_mins",
        "max_end": "last_active_time_student"
    })

    # change time unit to minutes
    target_schools_report["total_attempts_mins"] = target_schools_report["total_attempts_mins"] / 60

    # also get daily active user count
    dauc = target_students_df.groupby(["school", "date"]).agg({"login_name": "nunique"}).unstack().fillna(0)
    dauc.columns = dauc.columns.droplevel()
    dauc.columns.name = ""
    dauc.index.name = "School Code"
    dauc = dauc.reset_index()

    # target_schools_report = pd.merge(target_schools_report, target_teachers_stats, how="outer", on="school")
    target_schools_report = pd.merge(school_list, target_schools_report, how="inner", on="school")
    # target_schools_report[target_schools_report.select_dtypes(exclude=['datetime'])] = \
    #     target_schools_report[target_schools_report.select_dtypes(exclude=['datetime'])].fillna(0)

    inactive_schools = school_set - set(target_schools_report["school"])
    inactive_schools = school_list[school_list["school"].isin(inactive_schools)].reset_index(drop=True)
    last_attempt_time = get_school_last_attempt_time(inactive_schools["school"])
    inactive_schools = inactive_schools.merge(last_attempt_time, on="school", how="left")

    return target_schools_report, inactive_schools, dauc


def write_to_excel(schools_report, inactive_schools, dauc, school_type, start_date, end_date):
    """
    Write the report to an excel file.

    :param schools_report: df of target school report
    :param inactive_schools: df of inactive school
    :param dauc: df of daily active user count
    :param school_type: type of school ("Paid", "ICT", "Trial" or "All")
    :return: None. An excel report file will be created.
    """
    # rename columns for excel writing
    schools_report = schools_report.rename(columns={
        "school": "School Code",
        "chinese_name": "Chinese Name",
        "english_name": "English Name",
        "num_active_students": "# Active Students",
        "num_attempts": "# Attempts",
        "total_attempts_mins": "Attempt Duration (min)",
        "last_active_time_student": "Last Attempt Time"
    })
    inactive_schools = inactive_schools.rename(columns={
        "school": "School Code",
        "chinese_name": "Chinese Name",
        "english_name": "English Name",
        "last_active_time_student": "Last Attempt Time"
    })

    writer = pd.ExcelWriter('reports/{}_to_{}_{}_activity_info.xlsx'.format(start_date, end_date,
                                                                      school_type), engine='xlsxwriter')
    workbook = writer.book
    datetime_format = workbook.add_format({'num_format': '%Y-%m-%d" hh:mm:ss'})

    # First page
    schools_report.to_excel(writer, sheet_name="Activity Stats", startrow=1, index=False)
    inactive_schools.to_excel(writer, sheet_name="Activity Stats", startrow=len(schools_report) + 5,
                                   index=False)

    # writing header
    worksheet = writer.sheets["Activity Stats"]
    header_format = workbook.add_format({
        'bold': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': '76933C',
        'font_color': 'FFFFFF',
        'border_color': '0270C0',
    })
    worksheet.merge_range(
        0, 0, 0, schools_report.shape[1] - 1,
        'Active User Summary ({})'.format(school_type),
        header_format
    )

    inactive_header_format = workbook.add_format({
        'bold': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': 'A6A6A6',
        'font_color': 'FFFFFF',
        'border_color': 'A6A6A6',
    })
    worksheet.merge_range(
        len(schools_report) + 4, 0, len(schools_report) + 4, inactive_schools.shape[1] - 1,
        'Inactive Schools ({})'.format(school_type),
        inactive_header_format
    )
    writer.sheets['Activity Stats'].set_column(0, 0, 10)
    writer.sheets['Activity Stats'].set_column(1, 1, 20)
    writer.sheets['Activity Stats'].set_column(2, 2, 30)
    writer.sheets['Activity Stats'].set_column(3, 6, 18)

    # 2nd page
    dauc.to_excel(writer, sheet_name="Daily Active User Count", index=False)
    writer.sheets['Daily Active User Count'].set_column(0, dauc.shape[1], 10)

    writer.save()


if __name__ == "__main__":
    # arg parse for month mode or week mode
    parser = argparse.ArgumentParser(description='4LT Active User Report Creator.')
    parser.add_argument('--mode', default="week", help='Report mode: Must be one of "week" or "month".')
    args = parser.parse_args()

    # start and end days for the report
    if args.mode == "week":
        START_DATE = (datetime.today() - timedelta(7)).date().strftime("%Y-%m-%d")
        END_DATE = (datetime.today().date() - timedelta(1)).strftime("%Y-%m-%d")
    elif args.mode == "month":
        START_DATE = (datetime.today() - relativedelta(months=1) - timedelta(datetime.today().day - 1)).date().strftime("%Y-%m-%d")
        END_DATE = (datetime.today().date() - timedelta(datetime.today().day + 1)).strftime("%Y-%m-%d")
    else:
        raise argparse.ArgumentError('Mode can only be "week" or "month".')

    teachers_df = get_teacher_login_info(START_DATE, END_DATE)
    teachers_df = teachers_df[teachers_df["latest_login_time"] >= pd.to_datetime((datetime.now() - timedelta(30)).date())]
    teachers_df = teachers_df.reset_index(drop=True)

    students_df = get_students_attempts(START_DATE, END_DATE)
    students_df = students_df[~students_df["login_name"].isin(teachers_df["login_name"])]\
        .reset_index(drop=True)  # remove teacher's attempts

    # get paid schools info
    print("Generating reports for paid schools...")
    paid_schools = get_paid_schools().drop("is_primary_school", axis=1)
    paid_schools_set = set(paid_schools["school"])

    paid_schools_report, inactive_paid_schools, paid_dauc = get_schools_stats(teachers_df, students_df, paid_schools)
    # print(paid_schools_report)
    # print(inactive_paid_schools)
    # print(dauc)

    write_to_excel(paid_schools_report, inactive_paid_schools, paid_dauc, "Paid", START_DATE, END_DATE)

    # get ict schools info
    print("Generating reports for ICT schools...")
    ict_schools = get_ict_schools().drop("is_primary_school", axis=1)
    ict_schools_set = set(ict_schools["school"])

    ict_schools_report, inactive_ict_schools, ict_dauc = get_schools_stats(teachers_df, students_df, ict_schools)

    write_to_excel(ict_schools_report, inactive_ict_schools, ict_dauc, "ICT", START_DATE, END_DATE)

    # get trial schools info
    print("Generating reports for trial schools...")
    trial_schools = get_trial_schools().drop("is_primary_school", axis=1)
    trial_schools_set = set(trial_schools["school"])

    trial_schools_report, inactive_trial_schools, trial_dauc = get_schools_stats(teachers_df, students_df, trial_schools)

    write_to_excel(trial_schools_report, inactive_trial_schools, trial_dauc, "Trial", START_DATE, END_DATE)

    print("Done!")






