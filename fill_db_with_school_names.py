import pandas as pd
pd.set_option("display.max_columns", 20)
import numpy as np
import ast

import pymysql


PRIMARY_SYLLABUS = {'p1math', 'p2math', 'p3math', 'p4math', 'p5math', 'p6math',
                    'Demo_P1Maths', 'Demo_P2Maths', 'Demo_P3Maths', 'Demo_P4Maths', 'Demo_P5Maths', 'Demo_P6Maths',
                    'demo-p7', 'math_p5_new', 'math_p6_new', 'math_p3_new', 'math_p4_new',
                    'FSSAS_P4_Maths', 'maths_p4_eng', 'math_p1_new', 'math_p2_new',
                    'ychchtpsp4', 'ychchtpsp5', 'ychchtpsp6', 'maths_p5_eng'}

SECONDARY_SYLLABUS = {'f1_f3_math', 'f1maths', 'f2maths', 'f3maths', 'f5maths',
                      'f6maths', 'DSEmaths', 'demo_s1maths', 'demo_s2maths', 'demo_s3maths',
                      'S4MathsDemo', 'Demo_s5math', 'Demo_Secondary_from_F1_to_F6',
                      'f4_f6_math', 'ict_mock', 'LKCSS_S1', 'HKTLC_ICT', 'hktlc_ict_mock',
                      'S3_TSA', 'TSA_S3', 'DSEMathMockPackage'}


def parse_exam_curriculum_field(x):
    try:
        return ast.literal_eval(x)
    except SyntaxError:
        return []


def primary_secondary_counter(l):
    d = {"primary": 0, "secondary": 0}
    for i in l:
        if i in PRIMARY_SYLLABUS:
            d["primary"] += 1
        elif i in SECONDARY_SYLLABUS:
            d["secondary"] += 1
    return d


def primary_secondary_decider(d1, d2):
    if d1["primary"] > d1["secondary"] or d2["primary"] > d2["secondary"]:
        return "primary"
    elif d1["secondary"] > d1["primary"] or d2["secondary"] > d2["primary"]:
        return "secondary"
    else:
        return np.nan


if __name__ == "__main__":
    host = "starplatform.c2ueg0anncqg.ap-southeast-1.rds.amazonaws.com"
    user = "starplatform"
    password = "star2016"
    port = 3306
    db = "fsschool"
    db_conn = pymysql.connect(host, user=user,port=port, passwd=password, db=db)

    query = """
    UPDATE schools
    SET chinese_name = '{}', english_name = '{}'
    WHERE domain = '{}'
    """

    query2 = """
    INSERT INTO school_type (domain, is_primary_school)
    VALUES ('{}', '{}')
    """

    query3 = """
    INSERT INTO school_paid (domain, is_paid)
    VALUES ('{}', '{}')
    """

    # school_list_df = pd.read_sql(query, con=db_conn)
    school_list = pd.read_excel("data/school_list_by_type.xlsx")
    paid_school_list = pd.read_excel("data/paid_school_info.xlsx")
    school_list["english_name"] = school_list["english_name"].map(lambda x: x.replace("'", "''"))
    paid_school_list = set(paid_school_list["domain"])
    for ind, row in school_list.iterrows():
        # school_type = row["type"] if row["type"] in ["primary", "secondary"] else None
        # db_conn.query(query.format(row["chinese_name"], row["english_name"], row["domain"]))
        # if school_type is not None:
        #     db_conn.query(query2.format(row["domain"], 1 if school_type == "primary" else 0))
        is_paid = 1 if row["domain"] in paid_school_list else 0
        db_conn.query(query3.format(row["domain"], is_paid))
    db_conn.commit()


