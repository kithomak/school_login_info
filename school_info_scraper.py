import pandas as pd
pd.set_option("display.max_columns", 20)
from collections import Counter

from splinter import Browser
from splinter.driver.webdriver import EC, WebDriverWait
from selenium.webdriver.common.by import By


def check_school_type(browser, school_name):
    """
    Check if a school is a primary or secondary school.

    :param browser: webdriver to use
    :param school_name: name of school to be checked
    :return: school type: "primary" or "secondary" or "undecided"
    """
    # fool proof checking
    school_name = school_name.strip()
    if school_name[-2:] == "小學":
        return "primary"
    elif school_name[-2:] == "中學" or school_name[-2:] == "書院":
        return "secondary"

    # try wiki
    url = "https://zh.wikipedia.org/zh-hk/{}".format(school_name)
    browser.visit(url)

    ele = browser.find_by_tag("html")
    content = ele[0].text
    chunks = content.strip().split()
    try:
        idx = chunks.index("學校類別")
        school_type = chunks[idx + 1]
        if "小學" in school_type:
            return "primary"
        elif "中學" in school_type:
            return "secondary"
        else:
            return school_type
    except ValueError:
        for chunk in chunks:
            if "分類：" in chunk:
                num_primary_schools = chunk.count("小學")
                num_secondary_schools = chunk.count("中學")
                if num_primary_schools > num_secondary_schools:
                    return "primary"
                elif num_secondary_schools > num_primary_schools:
                    return "secondary"

    # if wiki can't help, try google search
    url = "https://www.google.com/search?q={}".format(school_name)
    browser.visit(url)

    ele = browser.find_by_tag("html")
    content = ele[0].text.lower()

    num_primary_schools = content.count("primary school")
    num_secondary_schools = content.count("secondary school") + content.count("high school")
    if num_primary_schools > num_secondary_schools:
        return "primary"
    elif num_secondary_schools > num_primary_schools:
        return "secondary"

    return "undecided"


if __name__ == "__main__":
    browser = Browser('chrome', headless=True)
    df = pd.read_excel("data/school_list.xlsx")
    df2 = df[["domain", "chinese_name", "english_name"]].dropna()
    df2 = df2[df2["chinese_name"] != "/"]
    school_names = list(df2["chinese_name"])
    school_types = []
    for school_name in school_names:
        school_type = check_school_type(browser, school_name)
        # print(school_name + ":", school_type)
        school_types.append(school_type)
    browser.quit()

    df = pd.DataFrame({"domain": list(df2["domain"]),
                       "chinese_name": school_names,
                       "english_name": list(df2["english_name"]),
                       "type": school_types})
    df.to_excel("data/pri_or_sec.xlsx", index=False)

    print(Counter(school_types))
